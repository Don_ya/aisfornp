\begin{thebibliography}{10}
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{\texttt{#1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi
\expandafter\ifx\csname href\endcsname\relax
  \def\href#1#2{#2} \def\path#1{#1}\fi

\bibitem{CorusOlivetoYazdani2018}
D.~Corus, P.~S. Oliveto, D.~Yazdani, Artificial immune systems can find
  arbitrarily good approximations for the {NP-hard} partition problem, in:
  Proc. of PPSN 2018, 2018, pp. 16--28.

\bibitem{ComputerSecurityApp}
S.~Forrest, A.~S. Perelson, L.~Allen, R.~Cherukuri, Self-nonself discrimination
  in a computer, in: Proc. of 1994 IEEE Symp. on Security and Privacy, 1994,
  pp. 202--212.

\bibitem{VirusDetectApp}
S.~Hedberg, Combating computer viruses: {IBM}'s new computer immune system,
  IEEE Parallel \& Distributed Technology: Systems \& Applications 4~(2) (1996)
  9--11.

\bibitem{AnamolyDetectApp}
D.~Dasgupta, N.~S. Majumdar, Anomaly detection in multidimensional data using
  negative selection algorithm, in: Proc. of CEC 2002, 2002, pp. 1039--1044.

\bibitem{Burnet1959}
F.~M. Burnet, The clonal selection theory of acquired immunity, Cambridge
  University Press, 1959.

\bibitem{decastro}
L.~N. de~Castro, F.~J.~V. Zuben, Learning and optimization using the clonal
  selection principle, IEEE Transactions on Evolutionary Computation 6~(3)
  (2002) 239--251.

\bibitem{kelsey}
J.~Kelsey, J.~Timmis, Immune inspired somatic contiguous hypermutation for
  function optimisation, in: Proc. of GECCO 2003, 2003, pp. 207--218.

\bibitem{CutelloTEVC}
V.~Cutello, G.~Nicosia, M.~Pavone, J.~Timmis, An immune algorithm for protein
  structure prediction on lattice models, IEEE Transactions on Evolutionary
  Computation 11~(1) (2007) 101--117.

\bibitem{JansenZarges2012}
T.~Jansen, C.~Zarges, Computing longest common subsequences with the {B-Cell
  Algorithm}, in: Proc. of ICARIS 2012, 2012, pp. 111--124.

\bibitem{JansenOlivetoZarges2011}
T.~Jansen, P.~S. Oliveto, C.~Zarges, On the analysis of the immune-inspired
  {B-Cell} algorithm for the vertex cover problem, in: Proc. of ICARIS 2011,
  2011, pp. 117--131.

\bibitem{CorusOlivetoYazdani2017}
D.~Corus, P.~S. Oliveto, D.~Yazdani, On the runtime analysis of the {O}pt-{IA}
  artificial immune system, in: Proc. of GECCO 2017, 2017, pp. 83--90.

\bibitem{DoerrGecco2017}
B.~Doerr, H.~P. Le, R.~Makhmara, T.~D. Nguyen, Fast genetic algorithms, in:
  Proc. of GECCO 2017, 2017, pp. 777--784.

\bibitem{OlivetoLehreNeumann2009}
P.~S. Oliveto, P.~K. Lehre, F.~Neumann, Theoretical analysis of rank-based
  mutation-combining exploration and exploitation, in: Proc. of CEC 2009, 2009,
  pp. 1455--1462.

\bibitem{CorusOlivetoTEVC}
D.~{Corus}, P.~S. {Oliveto}, Standard steady state genetic algorithms can
  hillclimb faster than mutation-only evolutionary algorithms, IEEE
  Transactions on Evolutionary Computation 22~(5) (2017) 720--732.

\bibitem{JumpTEVC2017}
D.-C. Dang, T.~Friedrich, T.~K{\"o}tzing, M.~S. Krejca, P.~K. Lehre, P.~S.
  Oliveto, D.~Sudholt, A.~M. Sutton, Escaping local optima using crossover with
  emergent diversity, IEEE Transactions on Evolutionary Computation 22~(3)
  (2018) 484--497.

\bibitem{DoerrDoerrEbel2015}
B.~Doerr, C.~Doerr, F.~Ebel, From black-box complexity to designing new genetic
  algorithms, Theoretical Computer Science 567 (2015) 87--104.

\bibitem{LehrWitt2012}
P.~K. Lehre, C.~Witt, Black-box search by unbiased variation, Algorithmica 64
  (2012) 623--642.

\bibitem{EasiestFunctions}
D.~Corus, J.~He, T.~Jansen, P.~S. Oliveto, D.~Sudholt, C.~Zarges, On easiest
  functions for mutation operators in bio-inspired optimisation, Algorithmica.
  78 (2016) 714--740.

\bibitem{GareyJohnson1979}
M.~R. Garey, D.~S. Johnson, Computers and Intractability: A Guide to the Theory
  of NP-Completeness, W. H. Freeman, 1979.

\bibitem{Pinedo2016}
M.~Pinedo, Scheduling: theory, algorithms, and systems, 5th Edition,
  Prentice-Hall, 2016.

\bibitem{Hayes2002}
B.~Hayes, The easiest hard problem, American Scientist 90 (2002) 113--117.

\bibitem{Witt2005}
C.~Witt, Worst-case and average-case approximations by simple randomized search
  heuristics, in: Proc. of STACS 2005, Springer-Verlag, 2005, pp. 44--56.

\bibitem{NeumannWitt2015}
F.~Neumann, C.~Witt, On the runtime of randomized local search and simple
  evolutionary algorithms for dynamic makespan scheduling, in: Proc. of
  IJCAI'15, AAAI Press, 2015, pp. 3742--3748.

\bibitem{Janeway2011}
C.~Janeway, Janeway's immunobiology, 8th Edition, Garland Science, 2011.

\bibitem{DrosteJansenWegener2002}
S.~Droste, T.~Jansen, I.~Wegener, On the analysis of the (1+1) evolutionary
  algorithm, Theoretical Computer Science 276~(1-2) (2002) 51--81.

\bibitem{OlivetoSudholt2014}
P.~S. Oliveto, D.~Sudholt, On the runtime analysis of stochastic ageing
  mechanisms, in: Proc. of GECCO 2014, 2014, pp. 113--120.

\bibitem{JansenZarges2011c}
T.~Jansen, C.~Zarges, On the role of age diversity for effective aging
  operators, Evolutionary Intelligence 4~(2) (2011) 99--125.

\bibitem{Witt2006}
C.~Witt, {Runtime analysis of the {$(\mu+1)$ EA} on simple pseudo-boolean
  functions}, Evolutionary Computation 14~(1) (2006) 65--86.

\bibitem{corus2016}
D.~Corus, P.~K. Lehre, F.~Neumann, M.~Pourhassan, A parameterised complexity
  analysis of bi-level optimisation with evolutionary algorithms, Evolutionary
  Computation 24~(1) (2016) 183--203.

\bibitem{sutton2014}
A.~M. Sutton, F.~Neumann, S.~Nallaperuma, Parameterized runtime analyses of
  evolutionary algorithms for the planar euclidean traveling salesperson
  problem, Evolutionary Computation 22~(4) (2014) 595--628.

\bibitem{chauhan2017}
A.~Chauhan, T.~Friedrich, F.~Quinzan, Approximating optimization problems using
  {EAs} on scale-free networks, in: Proc. of GECCO 2017, ACM, 2017, pp.
  235--242.

\bibitem{DOERR201312}
B.~Doerr, D.~Johannsen, T.~Kötzing, F.~Neumann, M.~Theile, More effective
  crossover operators for the all-pairs shortest path problem, Theoretical
  Computer Science 471 (2013) 12--26.

\bibitem{doerr2017time}
B.~Doerr, F.~Neumann, A.~M. Sutton, Time complexity analysis of evolutionary
  algorithms on random satisfiable {k-CNF} formulas, Algorithmica 78~(2) (2017)
  561--586.

\bibitem{neumann2018runtime}
F.~Neumann, A.~M. Sutton, Runtime analysis of evolutionary algorithms for the
  knapsack problem with favorably correlated weights, in: Proc. of PPSN~XV,
  Springer, 2018, pp. 141--152.

\bibitem{sutton2016superpolynomial}
A.~M. Sutton, Superpolynomial lower bounds for the (1+1) {EA} on some easy
  combinatorial problems, Algorithmica 75~(3) (2016) 507--528.

\bibitem{LengelerPPSN2018}
J.~Lengler, A general dichotomy of evolutionary algorithms on monotone
  functions, in: Proc. of PPSN 2018, 2018, pp. 3--15.

\bibitem{LehrTEVC2018}
D.~Corus, D.-C. Dang, A.~V. Eremeev, P.~K. Lehre, Level-based analysis of
  genetic algorithms and other search processes, IEEE Transactions on
  Evolutionary Computation 22~(5) (2018) 707--719.

\bibitem{Sutton2018}
A.~M. Sutton, Crossover can simulate bounded tree search on a fixed-parameter
  tractable optimization problem, in: Proc. of GECCO 2018, 2018, pp.
  1531--1538.

\bibitem{NeumannWitt2010}
F.~Neumann, C.~Witt, Bioinspired computation in combinatorial optimization,
  Springer-Verlag, 2010.

\bibitem{LandauRef}
T.~Cormen, C.~E. Leiserson, R.~L. Rivest, C.~Stein, Introduction to Algorithms,
  MIT Press, 2009.

\bibitem{Karp1972}
R.~Karp, Reducibility among combinatorial problems, Complexity of Computer
  Computations (1972) 85--103.

\bibitem{BrunoCoffmanSethi1974}
J.~Bruno, E.~G. Coffman, R.~Sethi, Scheduling independent tasks to reduce mean
  finishing-time, Communications of the ACM 17 (1974) 382--387.

\bibitem{Graham1969}
R.~L. Graham, Bounds on multiprocessing timing anamolies, SIAM Journal on
  Applied Mathematics 17 (1969) 416--429.

\bibitem{HochbaumShmoys1987}
D.~S. Hochbaum, D.~B. Shmoys, Using dual approximation algorithms for
  scheduling problems: theoretical and practical results, Journal of the ACM 34
  (1987) 144--162.

\bibitem{GhalamiGrosu2018}
L.~Ghalami, D.~Grosu, Scheduling parallel identical machines to minimize
  makespan:a parallel approximation algorithm, To appear in Journal of Parallel
  and Distributed Computing.

\bibitem{Sahni1976}
S.~K. Sahni, Algorithms for scheduling independent tasks, Journal of the ACM 23
  (1976) 116--127.

\bibitem{feller1968}
W.~Feller, An Introduction to Probability Theory and Its Applications, John
  Wiley \& Sons, 1968.

\bibitem{mitzenmacher2005probability}
M.~Mitzenmacher, E.~Upfal, Probability and computing: Randomized algorithms and
  probabilistic analysis, Cambridge university press, 2005.

\bibitem{DoerrNewTool}
B.~Doerr, Probabilistic tools for the analysis of randomized optimization
  heuristics, ArXiv e-prints.To appear in Theory of Randomised Serach
  Heuristics in dicrete Serach Spaces, Springer, 2018.
\newblock \href {http://arxiv.org/abs/1801.06733} {\path{arXiv:1801.06733}}.

\bibitem{OlivetoHeYao2011}
P.~S. Oliveto, X.~Yao, Runtime analysis of evolutionary algorithms for discrete
  optimisation, in: A.~Auger, B.~Doerr (Eds.), Theory of Randomized Search
  Heuristics: Foundations and Recent Developments, World Scientific, 2011,
  Ch.~2, pp. 21--52.

\bibitem{jansenbook}
T.~Jansen, Analyzing Evolutionary Algorithms: The Computer Science Perspective,
  Springer, 2013.

\bibitem{LehreOliveto2018}
P.~K. Lehre, P.~S. Oliveto, Theoretical analysis of stochastic search
  algorithms, in: Handbook of Heuristics, Springer International Publishing,
  2018.
\newblock \href {http://dx.doi.org/10.1007/978-3-319-07153-4_35-1}
  {\path{doi:10.1007/978-3-319-07153-4_35-1}}.

\bibitem{lenglerNew}
J.~Lengler, Drift analysis, ArXiv e-prints.To appear in Theory of Randomised
  Serach Heuristics in dicrete Serach Spaces, Springer, 2018.
\newblock \href {http://arxiv.org/abs/1712.00964} {\path{arXiv:1712.00964}}.

\end{thebibliography}
