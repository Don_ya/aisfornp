
\bitflip*
\begin{proof}
Picking a uniformly random permutation in which to flip the bits is equivalent to the random process where a number in $[n]$ is assigned to each bit position without replacement. 
W.l.o.g., we index the $k$ bit positions  of interest from $x_1$ to $x_k$. The probability that bit $x_1$ is assigned a number in $[m]$ is $m/n$. Given that $\ell$ numbers in $[m]$ have been assigned to bits $x_i$ for all $i$ in $[\ell]$,  the probability that the $(\ell +1)$th bit to be assigned a number from $[m]$ is at least $(m-\ell)/(n-\ell)$. Therefore, the 
 probability that $k$ specific bit mutations occur in the first $m$ 
mutation steps is at least $\prod_{j=0}^{k-1}\frac{m-j}{n-j}$. Since $n\geq 
m$, 
$\frac{m-j}{n-j}\geq \frac{m-k}{n-k}$, for all $j \in \{0,1,\ldots,k-1\}$, and 
consequently the series product is bounded from below by 
$\left(\frac{m-k+1}{n-k+1}\right)^{k}$
\end{proof}

\hypinterv*
\begin{proof}
Consider the input bitstring of the hypermutation operator $x_0$. If the hypermutation flips all $k$ bits where $s^0$ and $s^*$ differ in the first $m$ flips (which happens with probability at least 
$\left(\frac{m-k+1}{n-k+1}\right)^{k}\geq \left(\frac{m-|S|+1}{n-|S|+1}\right)^{k}$ due to Lemma~\ref{lem:bitflip}) and flips the $|S|-k$ bits where they agree in the last $m$ bit flips (which happens 
with probability  $\left(\frac{m-|S|+k+1}{n-|S|+k+1}\right)^{|S|-k} \geq \left(\frac{m-|S|+1}{n-|S|+1}\right)^{|S|-k}$ 
due to Lemma~\ref{lem:bitflip} ), all the solutions sampled in between will satisfy $s^i=s^*$. Although the two outcomes (flip positions of the $k$ and the $|S|-k$ bits) are not 
independent, each event implies that the number of assignable flip positions to the other set of bit positions is less while the number of satisfying flip positions is the same. Thus, the probability that both outcomes are observed in the same hypermutation operation is at least the product of their independent probabilities which can be bounded from below by $ \left(\frac{m-|S|+1}{n-|S|+1}\right)^{|S|}$.
\end{proof}

To prove Theorem~\ref{thm:hypmain}, we will use Serfling's bound on the 
hypergeometric distribution.

Consider a population $C:=\{c_1, \ldots, c_n\}$ consisting of $n$ elements, 
with $c_i \in R$ where $c_{min}$ and $c_{max}$ are the smallest and largest 
elements in $C$ respectively. Let $\bar{\mu}:= (1/n)\sum_{j=1}^{n}c_i$, be the 
population mean of $C$. Let $1\leq i \leq k \leq n$ and $X_i$ denote the $i$th 
draw without replacement from $C$ and $\bar{X}:=(1/k)\sum_{j=1}^{k} X_i$  the 
sample mean.

\begin{theorem}[Serfling \cite{serfling1974probability}]\label{thm:serfling}
 For $1\leq k \leq n$, and $\lambda>0$
 \[Prob\left\{  \sqrt{k}  (\bar{X} - \bar{\mu}) \geq \lambda \right\}\leq 
\exp\left(-\frac{2\lambda^2}{(1-f^{*}_{k})(c_{max}-c_{min})^{2}}\right)\] 
where $f^{*}_{k}:= \frac{k-1}{n}$. 
\end{theorem}

This theorem can be seen as the Chernoff bound for hypermutations. Chernoff 
bounds provide exponential probability bounds for Poisson 
distributions and are widely used in the runtime analysis of evolutionary 
algorithms where the standard bit mutation operator flips bits independently 
\cite{OlivetoHeYao2011}. Theorem~\ref{thm:serfling} on the other hand provides 
concentration bounds for the hypergeometric distribution which arises from the 
hypermutation operator which flips bits without replacement. 


\hypmain*


\begin{proof}
 For the input bitstring of hypermutation $x$ with $\left(\frac{1}{2} + 
a\right)n$ 1-bits, let the set of weights $C:=\{c_i, i\in [n]\}$ be defined as 
$c_i:=(-1)^{x_i}$ (i.e., $c_i=-1$ if $x_i=1$ and  $c_i=1$ if $x_i=0$). Thus, 
for permutation $\pi$ of bit-flips over $[n]$, the number of 1-bits after the $k$th 
mutation step is 
\[\left(\frac{1}{2} + a\right)n + \sum\limits_{j=1}^{k} c_{\pi_{j}}\]  
since flipping the position $i$ implies that the number of 1-bits changes by 
$c_i$.

Let $\bar{\mu}:= (1/n)\sum_{j=1}^{n}c_i = - 2 a $ be the population mean of 
$C$ and $\bar{X}:=(1/k)\sum_{j=1}^{k} c_{\pi_{j}}$ the sample mean . In 
order to have a solution with at least $n/2$ 1-bits at mutation step $k$, the 
following must hold:
\[
 \left(\frac{1}{2} + a\right)n + k \bar{X} \geq \frac{n}{2} \iff  \bar{X} \geq 
-\frac{a n}{k}  \iff  \bar{X} - \bar{\mu} \geq -\frac{a n}{k} + 2a \\
 \]

 For $k \geq n\frac{a}{2a-c}$,
\begin{align*}
 & \bar{X} - \bar{\mu} \geq -\frac{a n}{k} + 2a \implies 
 \bar{X} - \bar{\mu} \geq -\frac{an}{n\frac{a}{2a-c}} + 2a =c  \\
&\iff\\
&\sqrt{n\frac{a}{2a-c}}\left( \bar{X} - \bar{\mu}\right) \geq 
\sqrt{n\frac{a}{2a-c}} c.  
\end{align*}

Our claim follows from Theorem \ref{thm:serfling}, with sample mean $\bar{X}$, 
population mean $\bar{\mu}$, sample size $(n a) /(2a-c)$, population 
size $n$, $c_{min}=-1$ and $c_{max}=1$.
\begin{align*}
&Prob\left\{\sqrt{n\frac{a}{2a-c}} \left(\bar{X} - \bar{\mu}\right) \geq 
\sqrt{n\frac{a}{2a-c}} c  \right\} \\ 
 &\leq exp\left(- \frac{2 \left(\sqrt{n\frac{a}{2a-c}} 
c\right)^2}{\left(1-\left( \frac{n\frac{a}{2a-c} -1}{n}    
\right)\right)(1-(-1))^2}\right)=e^{-\Omega(n c^2)}
\end{align*}
\end{proof}

\hypmaincor*
\begin{proof}
We first note that the Theorem~\ref{thm:hypmain} also holds for the number of 
0-bits since the operator does not distinguish 1-bits and 0-bits.

The claim follows from the symmetry of the hypermutation operation. For any 
permutation $\pi$ over $[n]$ which designates the order of bits to be flipped 
by the hypermutation operator, there exists the reverse order $\pi^{'}$ such 
that $\pi_{i}=\pi^{'}_{n-i}$ for all $i\in [n]$. Thus, the number of 
permutations which start with the input bitstring $\neg x$, the complementary 
bitstring of $x$, and yield more than $n/2$ 0-bits after the 
$n\frac{a}{2a-c}$th bitflip is equal to the number of permutations which starts 
from $x$ and yields less than $n/2$ 1-bits before the $n- n\frac{a}{2a-c} = 
n\frac{a-c}{2a-c}$ th bit flip.  Theorem~\ref{thm:hypmain} implies the 
ratio of the number of the former permutations to the size of the whole 
permutation space is less than $e^{-\Omega(nc^2)}$. Hence, it must also be the 
case for the latter that the probability of observing any of them is in the 
same order.

Since, both the probability of having less than $n/2$ 1-bits before 
$n\frac{a-c}{2a-c}$th flip and more than $n/2$ 1-bits after $n\frac{a}{2a-c}$th 
bitflip is in the order of $e^{-\Omega(n c^2)}$, by union 
bound with probability $1-e^{-\Omega(n c^2)}$ the number of 1-bits will be 
exactly $n/2$ at least once in between.
\end{proof}
