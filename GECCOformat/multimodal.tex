\section{A Multimodal Instance Class} \label{sec:uniqueoptimum}
For the class of instances considered in the previous section it may be sufficient to escape from a single local optimum to optimise the problem exactly.
In this section we introduce a more complicated class of instances with several local optima.
Our aim is to highlight how AIS can efficiently overcome all of them. The instance class is defined as follows.

\begin{figure}
\includegraphics [scale=0.70]{newinstanse}
\caption{The global optimum of \NewInstanse}
\end{figure}

\begin{definition}
\label{def:is}
The class of partition instances $I_{s}$ are characterised by 
a number of jobs $n$ which is divisible by four, an even number 
$s=\Theta(1)$ of large jobs and the following processing times:
\begin{equation*}
p_i= 
\begin{cases}
      1+n^{-\lfloor \frac{s+1}{2}\rfloor-1}& \text{if } i\leq s 
\\
      \frac{s}{n}& \text{otherwise}
    \end{cases}
\end{equation*}
\end{definition}

The large jobs in an \NewInstanse instance can be divided into pairs with the same 
weight. E.g.,  $p_1=p_2= 1 + 1/n^2$ and $p_3=p_4=1+1/n^3$ for $s=4$.   


Like for the \WittGeneralisedInstance instance class,  an instance of \NewInstanse may have any constant number $s$ of large jobs.
Differently from \WittGeneralisedInstance, the number of local optima increases exponentially with $s$.

\begin{lemma}\label{lem:subopt}
% The optimum of \NewInstanse class of instances is a solution with one of each large 
% job from the same weight and exactly half of the small jobs. All other 
For any \NewInstanse instance, solutions which assign two large jobs with the 
same weight  to the same machine cannot be globally optimal.  
\end{lemma}

\begin{proof}
We will refer to solutions which assign one large job from each different 
weight to each machine as \emph{balanced}. A balanced solution which also 
assigns exactly half of the small jobs to each machine will have a makespan of 
$\sum_{i=1}^{n}{p_i}/2$, which is clearly optimal since the half of the total 
workload is a lower bound on the optimum in all partition problems. 

For any solution $x$ which is not balanced, let $D$ denote the 
difference between the load of the fuller machine and the emptier machine when 
the small jobs are ignored. Since the optimal solution has no 
discrepancy (both machines have the same workload), to obtain an optimal 
solution, it is necessary to compensate the difference $D$ by adding small jobs 
to the emptier machine.  
In order to add an extra workload of $D$ to the 
emptier machine, it is necessary to assign $D (n / s)$ small jobs to it. However, 
if $D (n / s)$ is not an integer, then after adding either $\lceil D n / 
s \rceil$ or $\lfloor D n / 
s\rfloor$ small jobs, the solution will have a discrepancy smaller than the 
weight of a small job. Since all the large jobs are already placed, no possible 
assignment of the remaining small jobs will yield an optimal solution (i.e., a 
solution with no discrepancy).

Now, we will show that $D (n / s)$ is not an integer. The measure $D$ can be 
divided into two additive terms as follows:

\[
 C +\sum_{i=2}^{(s/2) + 1 } \frac{c_i}{n^i}
\]

$C$ denotes the difference between the number of jobs in two machines and the 
term $c_i\in\{-2, 0, 2\}$ denotes whether the jobs with weight $1+1/n^i$ are 
both on the fuller machine (i.e., $2$), on different machines (i.e. $0$) or 
both on the emptier machine (i.e., $-2$). The sum of the $c_i/n^i$ is in the 
order of $O(1/n^2)$ and hence not an integer. Since, the number of jobs $n$ is 
divisible by four according to Definition~\ref{def:is}, the multiplicative 
factor $n/s$ is an integer \textcolor{red}{P: Let $n=100$ and $s=32$. Then $n$ is divisible by 4 and $s$ is even. But $n/s$ is not an integer. Maybe we need to define the instance class such that $n$ is divisible by $s$?}. Therefore, $C n/ s$ is an integer while  
$\sum_{i=2}^{(s/2) + 1 } \frac{c_i}{n^i} \frac{n}{s}$ is not. This establishes 
that $D n /s$ is not an integer and thus our claim.
\end{proof}


%
%\begin{proof}
%The randomly initialised solution does not assign the large jobs evenly among the machines with probability with probability $(1 - \binom{s}{s/2} 2^{-s+1})$. Since there are $n-s$small jobs  distribution of small jobs the fuller machine has less than $n/2 + \sqrt{n}$ jobs with overwhelmingly high probability due to Chernoff bounds. Thus, the discrepancy created by the small jobs is initially in the order of $1/\sqrt{n}$, so with overwhelmingly high probability the machine which has more large jobs is the fuller machine. If a small job is moved from the fuller machine to the emptier machine the offspring improves and is accepted as the new search point. If there are more than $(1/2 - 4/5s)(n-s)$ small jobs on the fuller machine then in each iteration with probability at least $e^{-1}(1/2 - 4/5s)(1-s/n)> 1/10e$ the current solution improves. : The expected time from initialisation until the number of small jobs on the fuller machine decrease to $(1/2 - 4/5s)(n-s)$  is  $ 8 e n / s $: For $c n$ steps the large jobs are untouched with probability $(1- (s/n))^{ c n } \geq e^{-c s}$: The probability that the large jobs are untouched for $ 8 e n /s$ is at least $  e^{-8e}$
%
%
%\end{proof}
%
%

\begin{lemma}\label{lem:locopt}
 If the current solution assigns  at least  $s/2 + 1$ large jobs and less than a $\frac{1}{2}-\frac{4}{5s}$ 
fraction of small jobs to the same machine, then the remaining time until \oneoneea finds the optimal solution is $n^{-\Omega(n)}$   with probability at least  $1-o(1)$. 
\end{lemma}

\begin{proof}
We first show that if the fuller machine has exactly $s/2 + 1$ large jobs and less than a $\frac{1}{2}-\frac{4}{5s}$ 
fraction of small jobs, then the algorithm requires exponential time with high probability to move one large job to the emptier machine.
Since by Lemma~\ref{lem:subopt} having equal number of large jobs on both machines is necessary for optimality, this will be sufficient to prove the claim once
we show that the algorithm reaches such a configuration with high probability.

With $(s/2) -1$ large jobs and at least   $ (\frac{1}{2}+\frac{4}{5s}) (n-s)$ small jobs assigned to the emptier machine, its
workload is at least,
\begin{align*}
& \frac{s}{2} - 1 +  \left(\frac{1}{2}+\frac{4}{5s}\right)(n-s) \frac{s}{n} %\\&
=\frac{s}{2} 
- 1 +  
s\left(\frac{1}{2}+\frac{4}{5s}) (1-\frac{s}{n}\right)  \\ 
&= \left(s-\frac{1}{5}\right) \left(1-o(1)\right).
\end{align*}
The load on the fuller machine is at most,
\begin{align*}
&\frac{s}{2}+ 1 + 3/n^2+  \left(\frac{1}{2}-\frac{4}{5s}\right)(n-s) 
\frac{s}{n} \\
&=\left(s + \frac{1}{5}\right) \left(1-o(1)\right)
\end{align*}
Hence, the difference between the loads  is $(2/5)(1-o(1))$.  If a single  large job is moved from the fuller machine to the emptier, then the makespan will be at least $s + 4/5$.  
In order to accept the new solution, a linear number of small jobs must be moved from the emptier machine to the fuller machine. Since the probability that SBM flips a linear number of bits is $n^{-\Omega(n)}$, it takes exponentially long expected time until a single large object is moved to the emptier machine. 
Moving more small jobs to the emptier machine can only decrease the discrepancy making it less likely for the emptier machine to gain another large job.  
%Since according to Lemma~\ref{lem:subopt} having equal number of large jobs on both machines is necessary for optimality, the lemma follows.

We conclude the proof by showing that with probability at least $1-o(1)$ the fuller machine will move large jobs to the emptier machine until it is left with  $s/2 + 1 $ large jobs
\textcolor{red}{(Or with a discrepancy of at most 1/2? i.e., which does not allow to move a large job to the emptier machine?)}.

The probability that more than one large job is moved in the same iteration is less than
$n ^{-2} \binom{s}{2} =O(n^{-2})$. The probability that such a move is observed 
in $O(n^{1+c})$ iterations is at most in the order of $O(n^{c-1})$ due to union 
bounds for any constant $0<c<1$.
If the number of large jobs in the fuller machine is  larger than $s/2 +1$ and the difference in loads is larger than $1/2$, then with probability at least $ ((s/2)n) (1-1/n)^{n-1}$, a single large job is moved from the fuller machine to the emptier machine. Conditional on that no mutation which moves multiple large jobs occurs, in  some $O(n^{1+c})$ time, either the fuller machine has exactly $s/2 + 1 $ large jobs on it, or the difference between the loads is strictly less than $1/2$, with overwhelmingly high probability, since the expected time until at most $s$ jobs are moved is $O(n)$. 
 
Overall, with probability $1-O(n^{c-1})$, the large jobs are moved one by one from the fuller 
machine to the emptier machine until there are only $s/2 +1$ of them left in the 
fuller machine.
%We will first bound the workload of the emptier machine. There are $(s/2) -1$ large jobs and at least   $ (\frac{1}{2}+\frac{4}{5s}) (n-s)$ small jobs assigned to it and the total workload is at least. 
%\begin{align*}
%&\geq \frac{s}{2} - 1 +  (\frac{1}{2}+\frac{4}{5s})(n-s) \frac{s}{n} \\&=\frac{s}{2} 
%- 1 +  
%s(\frac{1}{2}+\frac{4}{5s}) (1-\frac{s}{n})  \\ 
%&= \left(s-\frac{1}{5}\right) \left(1-o(1)\right)
%\end{align*}

%and 



\textcolor{red}{P:why do we need to say the following?} When there are at least $s/2 +2$ large jobs on the fuller machine then moving a large job to the emptier machine improves the solution and in at most $O(n)$ the number 
of large jobs on the fuller machine decreases to $s/2 +1$.


%The difference between the loads  is $(2/5)(1-o(1))$.  If a single  large job is moved from the fuller machine to the emptier, then the makespan will be at least $s + 4/5$.  In order to accept the new solution, a linear number of small jobs must be moved from the emptier machine to the fuller machine. Since the probability that SBM flips a linear number of bits is $n^{-\Omega(n)}$, it takes exponentially long time until a single large object is moved to the emptier machine. Since according to Lemma~\ref{lem:subopt} having equal number of large jobs on both machines is necessary for optimality, the lemma follows.
\end{proof}

%(x +1)      -   (x + 2/5 -sd1)
%
  %\item if there are at least $(\frac{1}{2}+\frac{4}{5s})(n-s)$ jobs on the 
%emptier machine for an 
%arbitrarily small constant, then the emptier machine has a workload of at 
%least:
%\begin{align*}
%&\frac{s}{2} - 1 +  (\frac{1}{2}+\frac{4}{5s})(n-s) \frac{s}{n} \\&=\frac{s}{2} 
%- 1 +  
%s(\frac{1}{2}+\frac{4}{5s}) (1-\frac{s}{n})  \\ 
%&= \left(s-\frac{1}{5}\right) \left(1-o(1)\right)
%\end{align*}
%
%\item $s/2 + 1$ large machines weigh at most $s/2 + 1 + 3/n^2$.
%\item The load on the fuller machine which has less than 
%$(\frac{1}{2}-\frac{4}{5s})(n-s)$ 
%small jobs is at most 
%\begin{align*}
%&\frac{s}{2}+ 1 + 3/n^2+  \left(\frac{1}{2}-\frac{4}{5s}\right)(n-s) 
%\frac{s}{n} \\
%&=\left(s + \frac{1}{5}\right) \left(1-o(1)\right)
%\end{align*}
  %
%
%\end{itemize}
%The difference between them is at most $(2/5)\left(1-o(1)\right)$ above 
%difference is at most $2/5$, which is at least $\Omega(1)$ smaller than 
%half the size of a large machine. In order to move the large machine it is 
%necessary to move $\Omega(1) n/s$ small jobs simultaneously which occurs with 
%probability $n^{-\Omega(n)}$.
 %
 %
 %\end{proof}

\begin{theorem}
The runtime of  the \oneoneea on  \NewInstanse is  $n^{-\Omega(n)}$  with probability at least $(1 - \binom{s}{s/2} 2^{-s}) e^{-8e} $. 
         %(or with high probability? for this we need to show it is really probable to end up in a local optimum which might be not the case)
\end{theorem}

[UNDER CONSTRUCTION]
\begin{proof}
The randomly initialised solution does not assign the large jobs evenly among the machines with probability with probability  $(1 - \binom{s}{s/2} 2^{-s})$.  
The fuller machine has less than $(n-s)/2 + \sqrt{n}$ small jobs with overwhelming probability due to Chernoff bounds. 
Thus, the discrepancy created by the small jobs is initially in the order of $O(1/\sqrt{n})$ and with overwhelming probability the machine which has most large jobs is the fuller machine in the initial solution.  
If there are more than $(1/2 - 4/5s)(n-s)$ small jobs on the fuller machine then in each iteration with probability at least $e^{-1}(1/2 - 4/5s)(1-s/n)> 1/10e$ the current solution improves by moving a single small job from the fuller to the emptier machine. : The expected time  until the number of small jobs on the fuller machine decrease to $(1/2 - 4/5s)(n-s)$  is  at most $ 8 e n / s$. The probability that the large jobs are untouched for $ 8 e n /s$ is at least $  e^{-8e}$. Once the fuller machine has less than $(1/2 - 4/5s)(n-s)$ small jobs, we do not require that the SBM does not move the large jobs. If there are more than $s/2+ c$ large jobs on the fuller machine, then at most $c/2$  \textcolor{red}{(or is it $c-1$??)} of them can be moved to the emptier machine.   The theorem statement follows from Lemma~\ref{lem:locopt}


\end{proof}


\subsection{Hypermutations}
\begin{theorem}
\oneoneais optimises \NewInstanse in $O(n^3)$. 
\end{theorem}

\begin{proof}
...
\end{proof}
\subsection{Ageing}

\begin{theorem}
\muoneeaageing optimises \NewInstanse class of instances in $O(?)$ steps in expectation for $\tau=\Omega(?)$ and $\mu=?$.
\end{theorem}
\begin{proof}
...
\end{proof}
